package com.mycompany.l03.capabilities;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class BrowserCapabilities {

    public static Capabilities getCapabilities(Browsers browser) {
        MutableCapabilities capabilities;
//        Proxy proxy = new Proxy()
//                .setProxyAutoconfigUrl("http://wpadvrn.t-systems.ru/wpad.dat");

        switch (browser) {
            case FIREFOX: {
//                FirefoxBinary firefoxBinary = new FirefoxBinary();
//                firefoxBinary.addCommandLineOptions("--

                capabilities = new FirefoxOptions()
                        //firefox configs - http://kb.mozillazine.org/About:config_entries
                        //firefox preferences - http://kb.mozillazine.org/Category:Preferences
                        .addPreference("app.update.auto", false)
                        .addPreference("app.update.enabled", false)
                        .addPreference("network.proxy.no_proxies_on", "localhost");
                capabilities.setCapability("--headless", true);
                capabilities.setCapability("window-size=1920x935", true);



//                        .setBinary(firefoxBinary)
                //   .setProxy(proxy);
                capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                break;
            }

            case CHROME: {
                //https://peter.sh/experiments/chromium-command-line-switches/
                capabilities = new ChromeOptions()
                        .addArguments("--start-maximized")
                        .addArguments("--ignore-certificate-errors");

                capabilities.setCapability("--headless", true);
                capabilities.setCapability("window-size=1920x935", true);



                //    .setProxy(proxy);
                break;
            }

            default:
                throw new IllegalArgumentException("Invalid browser: " + browser);
        }

        capabilities.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
        capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
        return capabilities;
    }

    public static DesiredCapabilities getDesiredCapabilities(Browsers browser) {
        DesiredCapabilities desiredCapabilities;
//

        switch (browser) {
            case FIREFOX: {


                desiredCapabilities = DesiredCapabilities.firefox();
                break;
            }

            case CHROME: {
                //https://peter.sh/experiments/chromium-command-line-switches/
                desiredCapabilities = DesiredCapabilities.chrome();

                break;
            }
            default:
                throw new IllegalArgumentException("Invalid browser: " + browser);
        }
        desiredCapabilities.setCapability("--headless", true);
        desiredCapabilities.setCapability("window-size=1920x935", true);
                return desiredCapabilities;
    }
}